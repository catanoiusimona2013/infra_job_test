# Infrastructure

## Pasi de utilizare

`env_var.sh` va contine project token-ul si alte secrete necesare in infrastructura.

Ca sa se poata clona repository-ul, se va folosi:
```
source env_var.sh
git clone https://$GITLAB_OPERATOR_USERNAME:$GITLAB_OPERATOR_TOKEN@www.licenta2024atm.com/gitlab/operator/infrastructure.git
```
