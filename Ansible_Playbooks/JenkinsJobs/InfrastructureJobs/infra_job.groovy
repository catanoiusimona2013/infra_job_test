pipelineJob('TerraformPipeline') {
    definition {
        cpsScm {
            scm {
                git {
                    remote {
                        url('https://www.licenta2024atm.com/gitlab/operator/infrastructure')
                        credentials('infrastructure-git-token')
                    }
                    branches('main')
                }
            }
            scriptPath('Jenkinsfile')
        }
    }
    triggers {
        gitlabPush {
            buildOnPushEvents(true)
            includeBranches('main')
        }
    }
}
