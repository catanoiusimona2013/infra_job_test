job('super-seed') {
        scm {
          git {
            remote {
              url ('https://www.licenta2024atm.com/gitlab/operator/infrastructure.git')
              credentials('infrastructure-git-token')
            }
            branches('*/main')
          }
        }
        steps {
          dsl {
            external('Ansible_Playbooks/JenkinsJobs/**/*.groovy')
            removeAction('DELETE')
          }
        }
      }