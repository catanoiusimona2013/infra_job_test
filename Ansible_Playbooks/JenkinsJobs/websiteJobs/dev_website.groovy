pipelineJob('website_dev_job') {
    definition {
        cpsScm {
            scm {
                git {
                    remote {
                        url('https://www.licenta2024atm.com/gitlab/developer/website.git')
                        credentials('website_registry_deploy')
                    }
                    branches('main-dev')
                }
            }
            scriptPath('Jenkinsfile')
        }
    }
    triggers {
        gitlabPush {
            buildOnPushEvents(true)
            includeBranches('main-dev')
        }
    }
}