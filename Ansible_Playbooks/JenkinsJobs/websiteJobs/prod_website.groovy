pipelineJob('website_prod_job') {
    definition {
        cpsScm {
            scm {
                git {
                    remote {
                        url('https://www.licenta2024atm.com/gitlab/developer/website.git')
                        credentials('website_registry_deploy')
                    }
                    branches('main')
                }
            }
            scriptPath('Jenkinsfile')
        }
    }
    triggers {
        gitlabPush {
            buildOnPushEvents(true)
            includeBranches('main')
        }
    }
}