- name: Obtain IP of node1
  hosts: localhost
  gather_facts: false
  tasks:
    - name: Set node1 IP as a variable
      ansible.builtin.set_fact:
        node1_ip: "{{ hostvars['node1']['ip'] }}"
    - name: Print node1 IP
      debug:
        var: node1_ip

- name: Get the config directory from one of the master node
  hosts: node1
  become: true
  tasks:
    - name: Copy kubeconfig file from master node
      ansible.builtin.fetch:
        src: "/etc/kubernetes/admin.conf"
        dest: "~/.kube/config"
        flat: true

- name: Configure Kubernetes access (kubectl) from http://127.0.0.1:6443 to master_ip:6443
  hosts: bastion
  become: true
  vars:
    node1_ip: "{{ hostvars['node1']['ip'] }}"
  tasks:
    - name: Retrieve K8s kubectl version
      ansible.builtin.uri:
        url: "https://dl.k8s.io/release/stable.txt"
        return_content: yes
      register: kubectl_version_output

    - name: Download kubectl binary
      ansible.builtin.get_url:
        url: "https://dl.k8s.io/release/{{ kubectl_version_output.content }}/bin/linux/amd64/kubectl"
        dest: "/usr/local/bin/kubectl"
        mode: "0755"

    - name: Create ~/.kube directory if it doesn't exist
      ansible.builtin.file:
        path: "/home/ubuntu/.kube"
        state: directory
        mode: '0755'
        owner: ubuntu
        group: ubuntu

    - name: Move kubeconfig file to bastion node
      ansible.builtin.copy:
        src: "~/.kube/config"
        dest: "/home/ubuntu/.kube/config"
        owner: ubuntu
        group: ubuntu

    - name: Update kubeconfig file with master IP
      ansible.builtin.replace:
        path: "/home/ubuntu/.kube/config"
        regexp: "server: https://.*:6443"
        replace: "server: https://{{ node1_ip }}:6443"

    # CREATE DEV USER FOR K8S (AUTHENTICATION)
    - name: Create certs directory
      ansible.builtin.file:
        path: "/certs"
        state: directory
        mode: '0755'
        owner: ubuntu
        group: ubuntu

    - name: Generate key for dev user
      ansible.builtin.openssl_privatekey:
        path: "/certs/dev.key"
        size: 2048
        owner: ubuntu
        group: ubuntu

    - name: Create CertificateSigningRequest for dev user
      ansible.builtin.shell: |
        cat <<EOF | kubectl apply -f -
        apiVersion: certificates.k8s.io/v1
        kind: CertificateSigningRequest
        metadata:
          name: dev
        spec:
          request: $(openssl req -new -key /certs/dev.key -subj '/CN=dev' | base64 | tr -d "\n")
          signerName: kubernetes.io/kube-apiserver-client
          usages:
          - client auth
        EOF
      become_user: ubuntu

    - name: Approve CertificateSigningRequest for dev user
      ansible.builtin.shell: "kubectl certificate approve dev"
      become_user: ubuntu

    - name: Retrieve and save dev certificate
      ansible.builtin.shell:
        cmd: "kubectl get csr dev -o jsonpath='{.status.certificate}'| base64 -d > /certs/dev.crt"
      become_user: ubuntu

    - name: Set ownership of dev certificate file
      ansible.builtin.file:
        path: "/certs/dev.crt"
        owner: ubuntu
        group: ubuntu

    - name: Configure kubectl for dev user
      ansible.builtin.shell:
        cmd: "kubectl config set-credentials dev --client-key=/certs/dev.key --client-certificate=/certs/dev.crt --embed-certs=true"
      become_user: ubuntu

    - name: Create dev user context
      ansible.builtin.shell:
        cmd: "kubectl config set-context dev-context --cluster=cluster.local --user=dev --namespace=dev"
      become_user: ubuntu

    # CREATE PROD USER FOR K8S (AUTHENTICATION)
    - name: Generate key for prod user
      ansible.builtin.openssl_privatekey:
        path: "/certs/prod.key"
        size: 2048
        owner: ubuntu
        group: ubuntu

    - name: Create CertificateSigningRequest for dev user
      ansible.builtin.shell: |
        cat <<EOF | kubectl apply -f -
        apiVersion: certificates.k8s.io/v1
        kind: CertificateSigningRequest
        metadata:
          name: prod
        spec:
          request: $(openssl req -new -key /certs/prod.key -subj '/CN=prod' | base64 | tr -d "\n")
          signerName: kubernetes.io/kube-apiserver-client
          usages:
          - client auth
        EOF
      become_user: ubuntu

    - name: Approve CertificateSigningRequest for prod user
      ansible.builtin.shell: "kubectl certificate approve prod"
      become_user: ubuntu

    - name: Retrieve and save prod certificate
      ansible.builtin.shell:
        cmd: "kubectl get csr prod -o jsonpath='{.status.certificate}'| base64 -d > /certs/prod.crt"
      become_user: ubuntu

    - name: Set ownership of prod certificate file
      ansible.builtin.file:
        path: "/certs/prod.crt"
        owner: ubuntu
        group: ubuntu

    - name: Configure kubectl for prod user
      ansible.builtin.shell:
        cmd: "kubectl config set-credentials prod --client-key=/certs/prod.key --client-certificate=/certs/prod.crt --embed-certs=true"
      become_user: ubuntu

    - name: Create prod user context
      ansible.builtin.shell:
        cmd: "kubectl config set-context prod-context --cluster=cluster.local --user=prod --namespace=prod"
      become_user: ubuntu

- name: Delete ~/.kube from local
  hosts: localhost
  gather_facts: false
  tasks:
    - name: Delete kubeconfig file from local machine
      ansible.builtin.file:
        path: ~/.kube/config
        state: absent
