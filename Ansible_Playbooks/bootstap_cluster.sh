#!/bin/bash

#am creat un .sh deoarece nu pot sa apelez un playbook din alt playbook in Ansible si am nevoie sa fac anumite modificari inainte
#de a rula playbook-ul de Kubespray (vreau sa automatizez instalarea de cluster cu totul)
source env_var.sh
ansible-playbook ./Ansible_Playbooks/Cluster/init_kubespray.yaml
cd ./Ansible_Playbooks/Cluster/kubespray
ansible-playbook --become --become-user=root -i ./inventory/${CLUSTER_NAME}/hosts.ini cluster.yml
cd ../../..
ansible-playbook -i ./Terraform/hosts_cluster.ini -i ./Terraform/hosts.ini Ansible_Playbooks/Cluster/main.yaml