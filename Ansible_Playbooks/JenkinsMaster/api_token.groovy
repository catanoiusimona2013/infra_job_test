// set_api_token.groovy
import hudson.model.User
import jenkins.security.ApiTokenProperty
def token = System.getenv("API_TOKEN")
def user = User.get('admin')
user.getProperty(ApiTokenProperty.class).tokenStore.addFixedNewToken("auto-token", token)
user.save()