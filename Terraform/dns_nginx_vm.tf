#Create jmphost instance based on jump host instance
module "dns_nginx" {
    source="./modules/instance_module"
    instance_name="dns_nginx_vm"
    image_id="59b03698-e42c-4f00-8aeb-5e82aec71023" #Ubuntu
    image_username = "ubuntu"
    flavor_name="m1.tiny"
    key_pair="dns_nginx_vm"
    instance_security_groups_name=["default"]
    script_path="/scripts/init.sh"
    port_name="dns_nginx_port"
    port_security_group_ids=["c70c415a-de02-4d84-9a1b-056707381dbe"] #default security group
    network_id=openstack_networking_network_v2.main_network.id
    subnet_id=openstack_networking_subnet_v2.workspace_subnet.id
    kubespray_groups = ""
}

resource "openstack_networking_floatingip_v2" "nginx_floating_ip" {
  pool="ext_net"
  description="nginx_floating_ip"
}

# Associate the floating IP with the nginx instance
resource "openstack_compute_floatingip_associate_v2" "nginx_floating_ip_association" {
  floating_ip = openstack_networking_floatingip_v2.nginx_floating_ip.address
  instance_id = module.dns_nginx.instance_id
}

output "dns_nginx_instance_ip" {
  value = module.dns_nginx.instance_ip
}
