#Cluster network
variable "network_name" {
  description = "Name of the network"
  type        = string
}

variable "subnet_name" {
  description = "Name of the subnet"
  type        = string
}

variable "cidr" {
  description = "CIDR for the subnet"
  type        = string
}

variable "dns_values" {
  description = "List of DNS nameservers"
  type        = list(string)
}

#Cluster settings
variable "cluster_name" {
  description = "Name of the cluster"
  default = "k8s"
  type        = string
}

variable "number_of_k8s_masters" {
  description = "Name of master nodes"
  default = 3
}

variable "number_of_k8s_nodes" {
  description = "Name of worker nodes"
  default = 2
}

variable "public_key_path" {
  description = "Public key used to access the nodes of the cluster from the bastion"
  default = "../../../VM_Private_Keys/cluster-key.pem.pub"
  type        = string
}

#Master settings
variable "master_flavour" {
  description = "Flavour to be used for master nodes"
  type        = string
}

variable "master_image_id"{
  description = "Image id for the master VM"
  type        = string
}

variable "master_image_username" {
  description = "Username for the choosen VM image"
  type        = string
}

variable "master_security_group" {
  description = "Security group name for master nodes"
  type        = list(string)
}

variable "master_port_security_group" {
  description = "Security group name for master ports"
  type        = list(string)
}

variable "master_kubespray_groups" {
  description = "Master kubespray groups"
  type        = string
}

variable "master_script_path" {
  description = "Master VM initialisaton script path"
  type        = string
}

#Master IPS
locals {
  master_ips = [
    for instance in module.k8s_masters : instance.instance_ip
  ]
}


#Worker settings
variable "workers_flavour" {
  description = "Flavour to be used for workers nodes"
  type        = string
}

variable "worker_image_id"{
  description = "Image id for the worker VM"
  type        = string
}

variable "worker_image_username" {
  description = "Username for the choosen VM image"
  type        = string
}

variable "worker_security_group" {
  description = "Security group name for worker nodes"
  type        = list(string)
}

variable "worker_port_security_group" {
  description = "Security group name for worker ports"
  type        = list(string)
}

variable "worker_kubespray_groups" {
  description = "Worker kubespray groups"
  type        = string
}

variable "worker_script_path" {
  description = "Worker VM initialisaton script path"
  type        = string
}

#Worker IPS
locals {
  worker_ips = [
    for instance in module.k8s_workers : instance.instance_ip
  ]
}

#NFS settings
variable "nfs_flavour" {
  description = "Flavour to be used for workers nodes"
  type        = string
}

variable "nfs_image_id"{
  description = "Image id for the worker VM"
  type        = string
}

variable "nfs_image_username" {
  description = "Username for the choosen VM image"
  type        = string
}

variable "nfs_security_group" {
  description = "Security group name for worker nodes"
  type        = list(string)
}

variable "nfs_port_security_group" {
  description = "Security group name for worker ports"
  type        = list(string)
}

variable "nfs_kubespray_groups" {
  description = ""
  type        = string
}

variable "nfs_script_path" {
  description = "Worker VM initialisaton script path"
  type        = string
}

#NFS IP ADDRESS
locals {
  nfs_ip = module.nfs_server.instance_ip
}