output "subnet_id" {
  description = "ID of the created subnet"
  value       = module.cluster_network.subnet_id
}

output "network_id" {
  description = "ID of the created network"
  value       = module.cluster_network.network_id
}

output "master_ips" {
  description = "List of the master IP addresses"
  value = local.master_ips
}

output "worker_ips" {
  description = "List of the worker IP addresses"
  value = local.worker_ips
}

output "nfs_ip" {
  description = "IP addresses of the NFS server"
  value = local.nfs_ip
}

output "master_image_username" {
  description = "VM username used for ssh"
  value = var.master_image_username
}

output "worker_image_username" {
  description = "VM username used for ssh"
  value = var.worker_image_username
}

output "nfs_image_username" {
  description = "VM username used for ssh"
  value = var.nfs_image_username
}

output "cluster_cidr" {
  description = "Cluster cidr needed for NFS server"
  value = var.cidr
}