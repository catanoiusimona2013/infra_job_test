terraform {
required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.51.1"
    }
  }
}

module "cluster_network" {
  source = "../network_module"
  network_name = var.network_name
  subnet_name  = var.subnet_name
  cidr         = var.cidr
  dns_values   = var.dns_values
}

#create ssh key to be used to connect from bastion to the cluster VMs
data "local_file" "public_key" {
  filename = var.public_key_path
}

resource "openstack_compute_keypair_v2" "k8s_key_pair" {
  name       = "${var.cluster_name}-key"
  public_key = chomp(data.local_file.public_key.content)
}

#Create masters
module "k8s_masters" {
    source="../instance_module"
    count = var.number_of_k8s_masters
    instance_name="${var.cluster_name}-master-${count.index + 1}"
    image_id=var.master_image_id
    image_username = var.master_image_username
    flavor_name=var.master_flavour
    key_pair=openstack_compute_keypair_v2.k8s_key_pair.name
    instance_security_groups_name=var.master_security_group
    script_path=var.master_script_path
    port_name="${var.cluster_name}-master-${count.index + 1}-port"
    port_security_group_ids=var.master_port_security_group
    network_id=module.cluster_network.network_id
    subnet_id=module.cluster_network.subnet_id
    kubespray_groups = var.master_kubespray_groups
}

#Create workers
module "k8s_workers" {
    source="../instance_module"
    count = var.number_of_k8s_nodes
    instance_name="${var.cluster_name}-worker-${count.index + 1}"
    image_id=var.worker_image_id
    image_username = var.worker_image_username
    flavor_name=var.workers_flavour
    key_pair=openstack_compute_keypair_v2.k8s_key_pair.name
    instance_security_groups_name=var.worker_security_group
    script_path= var.worker_script_path
    port_name="${var.cluster_name}-worker-${count.index + 1}-port"
    port_security_group_ids=var.worker_port_security_group
    network_id=module.cluster_network.network_id
    subnet_id=module.cluster_network.subnet_id
    kubespray_groups = var.worker_kubespray_groups
}

#Create NFS server
module "nfs_server" {
    source="../instance_module"
    instance_name="nfs-server"
    image_id=var.nfs_image_id
    image_username = var.nfs_image_username
    flavor_name=var.nfs_flavour
    key_pair=openstack_compute_keypair_v2.k8s_key_pair.name
    instance_security_groups_name=var.nfs_security_group
    script_path= var.nfs_script_path
    port_name="nfs-port"
    port_security_group_ids=var.nfs_port_security_group
    network_id=module.cluster_network.network_id
    subnet_id=module.cluster_network.subnet_id
    kubespray_groups = var.nfs_kubespray_groups
}