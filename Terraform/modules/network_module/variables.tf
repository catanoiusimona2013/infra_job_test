variable "network_name" {
  description = "Name of the network"
  type        = string
}

variable "subnet_name" {
  description = "Name of the subnet"
  type        = string
}

variable "cidr" {
  description = "CIDR for the subnet"
  type        = string
}

variable "dns_values" {
  description = "List of DNS nameservers"
  type        = list(string)
}