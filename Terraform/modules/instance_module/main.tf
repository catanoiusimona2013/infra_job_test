terraform {
required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.51.1"
    }
  }
}

resource "openstack_networking_port_v2" "instance_port"{
    name                 = var.port_name
    network_id           = var.network_id
    admin_state_up       = true
    security_group_ids   = var.port_security_group_ids
    fixed_ip {
    subnet_id = var.subnet_id
  }
}

resource "openstack_compute_instance_v2" "instance_vm" {
  name              = var.instance_name
  image_id          = var.image_id
  flavor_name       = var.flavor_name
  key_pair          = var.key_pair
  security_groups   = var.instance_security_groups_name
  user_data         = file("${path.module}/${var.script_path}")

  network {
    port = openstack_networking_port_v2.instance_port.id
  }

  metadata = {
    kubespray_groups = var.kubespray_groups
  }
}

