output "instance_ip" {
  value = openstack_compute_instance_v2.instance_vm.access_ip_v4
}

output "image_username" {
  value = var.image_username
}

output "instance_id" {
  value = openstack_compute_instance_v2.instance_vm.id
}