#Variable definition
variable "port_name" {}
variable "port_security_group_ids" {}
variable "instance_name" {}
variable "image_id" {}
variable "image_username" {}
variable "flavor_name" {}
variable "key_pair" {}
variable "instance_security_groups_name" {}
variable "script_path" {}
variable "network_id" {}
variable "subnet_id" {}
variable "kubespray_groups" {}