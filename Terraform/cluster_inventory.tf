data "template_file" "ansible_cluster_inventory_template" {
    template=templatefile("templates/hosts_cluster.cfg.tpl",{
      #bastion template vars
      ip_address_jmphost="${openstack_networking_floatingip_v2.jmphost_floating_ip.address}"
      os_default_user_jmphost="${module.jmphost.image_username}"
      nfs_server_user="${module.cluster.nfs_image_username}"
      nfs_ip="${module.cluster.nfs_ip}"
      cluster_cidr="${module.cluster.cluster_cidr}"
      nodes = concat([
        for ip in module.cluster.master_ips : {
          ip   = ip
          user = module.cluster.master_image_username
        }
      ], [
        for ip in module.cluster.worker_ips : {
          ip   = ip
          user = module.cluster.worker_image_username
        }
      ])
      master_count = length(module.cluster.master_ips)
    })
    depends_on = [         
        module.jmphost,
        module.cluster,
        openstack_compute_floatingip_associate_v2.jmphost_floating_ip_association,
    ]
}

resource "null_resource" "ansible_cluster_inventory_template" {
    triggers = {
      template_rendered = "${data.template_file.ansible_cluster_inventory_template.rendered}"
    }
    provisioner "local-exec" {
      command = <<-EOT
        cat <<EOF > hosts_cluster.ini
        ${data.template_file.ansible_cluster_inventory_template.rendered}
        EOF
      EOT
    }
}