data "template_file" "ansible_jenkins_inventory_template" {
    template="${file("templates/hosts_jenkins.cfg")}"
    depends_on = [         
        module.dns_nginx,
        module.jmphost,
        module.jenkins,
        module.jenkins_agent,
        openstack_compute_floatingip_associate_v2.jmphost_floating_ip_association,
        openstack_compute_floatingip_associate_v2.nginx_floating_ip_association,
    ]
    vars = {
      ip_address_jenkins="${module.jenkins.instance_ip}"
      ip_address_jenkins_agent="${module.jenkins_agent.instance_ip}"
      ip_address_jmphost="${openstack_networking_floatingip_v2.jmphost_floating_ip.address}"
      private_ip_address_nginx_dns="${module.dns_nginx.instance_ip}"
      public_ip_address_nginx_dns="${openstack_networking_floatingip_v2.nginx_floating_ip.address}"
      
      os_default_user_jenkins="${module.jenkins.image_username}"
      os_default_user_jenkins_agent="${module.jenkins_agent.image_username}"
      os_default_user_jmphost="${module.jmphost.image_username}"
      os_default_user_dns_nginx="${module.dns_nginx.image_username}"
    }
}

resource "null_resource" "ansible_jenkins_inventory_template" {
    triggers = {
      template_rendered = "${data.template_file.ansible_jenkins_inventory_template.rendered}"
    }
    provisioner "local-exec" {
      command = <<-EOT
        cat <<EOF > hosts_jenkins.ini
        ${data.template_file.ansible_jenkins_inventory_template.rendered}
        EOF
      EOT
    }
}