#Create jmphost instance based on jump host instance
module "jmphost" {
    source="./modules/instance_module"
    instance_name="jump_host_vm"
    image_id="59b03698-e42c-4f00-8aeb-5e82aec71023" #Ubuntu
    image_username = "ubuntu"
    flavor_name="m1.small"
    key_pair="jmp_host_vm"
    instance_security_groups_name=["default",openstack_networking_secgroup_v2.sec-gr-bastion.name]
    script_path="/scripts/init.sh"
    port_name="jmp_host_port"
    port_security_group_ids=["c70c415a-de02-4d84-9a1b-056707381dbe",openstack_networking_secgroup_v2.sec-gr-bastion.id] #default security group + bastion security group
    network_id=openstack_networking_network_v2.main_network.id
    subnet_id=openstack_networking_subnet_v2.workspace_subnet.id
    kubespray_groups = "bastion"
}

resource "openstack_networking_floatingip_v2" "jmphost_floating_ip" {
  pool="ext_net"
  description="jmphost_floating_ip"
}

# Associate the floating IP with the jump host instance
resource "openstack_compute_floatingip_associate_v2" "jmphost_floating_ip_association" {
  floating_ip = openstack_networking_floatingip_v2.jmphost_floating_ip.address
  instance_id = module.jmphost.instance_id
}

output "jmphost_instance_ip" {
  value = module.jmphost.instance_ip
}
