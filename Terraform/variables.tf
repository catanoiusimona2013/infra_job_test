variable "openstack_username" {
  description = "Openstack username"
  type        = string
}

variable "openstack_password" {
  description = "Openstack password"
  type        = string
}

variable "openstack_cert_path" {
  description = "Openstack certificate path"
  type        = string
}

variable "dns_values" {
    description = "List of the available dns servers"
    type=list(string)
    default = ["8.8.8.8","8.8.4.4"] 
    
}

variable "workspace_network" {
    description="Configuration of the workspace network"
    type=map(string)
    default={
        subnet_name="workspace_subnet"
        cidr="192.168.20.0/24"
    }
}

variable "ext_net_id" {
  description="Id of the exterior network"
  type=string
  default="d1069db2-47d1-42bd-b2a6-21e06e53b0c4"
}

variable "gitlab_token" {
  type=string
  description = "Gitlab Token to communicate with Terraform"
}