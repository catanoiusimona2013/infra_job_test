[all]
%{ for idx, node in nodes }node${idx + 1} ansible_host=${node.ip} ip=${node.ip} ansible_user=${node.user}
%{ endfor }
[all:vars]
ansible_ssh_common_args='-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o ProxyCommand="ssh -i ./VM_Private_Keys/jmp_host_vm.pem -W %h:%p ${os_default_user_jmphost}@195.242.244.111" -i ./VM_Private_Keys/cluster-key.pem'

[bastion]
bastion ansible_host=${ip_address_jmphost} ansible_user=${os_default_user_jmphost} ansible_ssh_common_args='-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i ./VM_Private_Keys/jmp_host_vm.pem'

[kube_control_plane]
%{ for idx, node in nodes ~}%{ if idx < master_count ~}node${idx + 1}
%{ endif }%{ endfor }
[etcd]
%{ for idx, node in nodes ~}%{ if idx < master_count ~}node${idx + 1}
%{ endif }%{ endfor }
[kube-node]
%{ for idx, node in nodes ~}%{ if idx >= master_count ~}node${idx + 1}
%{ endif }%{ endfor }
[no_floating]
%{ for idx, node in nodes }node${idx + 1} 
%{ endfor }
[calico-rr]

[nfs_server]
nfs_server ansible_host=${nfs_ip} ip=${nfs_ip} ansible_user=${nfs_server_user} cidr=${cluster_cidr}

[k8s-cluster:children]
kube_control_plane
kube-node
calico_rr