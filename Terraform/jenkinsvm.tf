#Create jenkins_master instance based on module instance
module "jenkins" {
    source="./modules/instance_module"
    instance_name="jenkins_master"
    image_id="59b03698-e42c-4f00-8aeb-5e82aec71023" #Ubuntu
    image_username = "ubuntu"
    flavor_name="m1.medium"
    key_pair="jenkinsvm"
    instance_security_groups_name=["default"]
    script_path="/scripts/init.sh"
    port_name="jenkins-port"
    port_security_group_ids=["c70c415a-de02-4d84-9a1b-056707381dbe"] #default security group
    network_id=openstack_networking_network_v2.main_network.id
    subnet_id=openstack_networking_subnet_v2.workspace_subnet.id
    kubespray_groups = ""
}

#Create jenkins_agent instance
module "jenkins_agent" {
    source="./modules/instance_module"
    instance_name="jenkins_agent"
    image_id="59b03698-e42c-4f00-8aeb-5e82aec71023" #Ubuntu
    image_username = "ubuntu"
    flavor_name="m1.medium"
    key_pair="jenkins_agent"
    instance_security_groups_name=["default"]
    script_path="/scripts/init_centos.sh"
    port_name="jenkins-agent-port"
    port_security_group_ids=["c70c415a-de02-4d84-9a1b-056707381dbe"] #default security group
    network_id=openstack_networking_network_v2.main_network.id
    subnet_id=openstack_networking_subnet_v2.workspace_subnet.id
    kubespray_groups = ""
}

output "jenkins_instance_ip" {
  value = module.jenkins.instance_ip
}

output "jenkins_agent_instance_ip" {
  value = module.jenkins_agent.instance_ip
}
