data "template_file" "ansible_inventory_template" {
    template="${file("templates/hosts.cfg")}"
    depends_on = [ 
        module.dns_nginx,
        module.jmphost,
        module.gitlab,
        openstack_compute_floatingip_associate_v2.jmphost_floating_ip_association,
        openstack_compute_floatingip_associate_v2.nginx_floating_ip_association,
    ]
    vars = {
      private_ip_address_nginx_dns="${module.dns_nginx.instance_ip}"
      public_ip_address_nginx_dns="${openstack_networking_floatingip_v2.nginx_floating_ip.address}"
      ip_address_jmphost="${openstack_networking_floatingip_v2.jmphost_floating_ip.address}"
      ip_address_gitlab="${module.gitlab.instance_ip}"
      
      os_default_user_dns_nginx="${module.dns_nginx.image_username}"
      os_default_user_jmphost="${module.jmphost.image_username}"
      os_default_user_gitlab="${module.gitlab.image_username}"
    }
}

resource "null_resource" "ansible_inventory_template" {
    triggers = {
      template_rendered = "${data.template_file.ansible_inventory_template.rendered}"
    }
    provisioner "local-exec" {
      command = <<-EOT
        cat <<EOF > hosts.ini
        ${data.template_file.ansible_inventory_template.rendered}
        EOF
      EOT
    }
}