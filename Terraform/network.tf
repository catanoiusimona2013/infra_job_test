#network
resource "openstack_networking_network_v2" "main_network" {
  name = "main_network"
}

#subnet
resource "openstack_networking_subnet_v2" "workspace_subnet" {
  name            = var.workspace_network["subnet_name"]
  network_id      = openstack_networking_network_v2.main_network.id
  cidr            = var.workspace_network["cidr"]
  dns_nameservers = var.dns_values
}

#router
resource "openstack_networking_router_v2" "main_network_router" {
    name="main-router"
    external_network_id = var.ext_net_id
}

#router interface
resource "openstack_networking_router_interface_v2" "main_network_interface"{
    router_id = openstack_networking_router_v2.main_network_router.id 
    subnet_id = openstack_networking_subnet_v2.workspace_subnet.id
}

#Network security groups
resource "openstack_networking_secgroup_v2" "sec-gr-bastion" {
  name                 = "bastion"
  description          = "Bastion Rules"
  delete_default_rules = true
}

resource "openstack_networking_secgroup_v2" "sec-gr-k8s-master" {
  name                 = "k8s-master"
  description          = "Kubernetes Master Rules"
  delete_default_rules = true
}

resource "openstack_networking_secgroup_v2" "sec-gr-k8s-worker" {
  name                 = "k8s-worker"
  description          = "Kubernetes Worker Rules"
  delete_default_rules = true
}

resource "openstack_networking_secgroup_v2" "sec-gr-nfs-server" {
  name                 = "nfs-server"
  description          = "Kubernetes NFS Server Rules"
  delete_default_rules = true
}

#master Security groups -> porturi deschise: 6443 de la oricine; ipv4 la oricine; ipv4 de la oricine din cluster(de la alte master-uri si noduri)
resource "openstack_networking_secgroup_rule_v2" "master-rule1" {
  ethertype = "IPv4"
  security_group_id = openstack_networking_secgroup_v2.sec-gr-k8s-master.id
  direction         = "ingress"
  protocol          = "tcp"
  port_range_min    = 6443
  port_range_max    = 6443
  remote_ip_prefix  = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_rule_v2" "master-rule2" {
  ethertype = "IPv4"
  security_group_id = openstack_networking_secgroup_v2.sec-gr-k8s-master.id
  direction         = "egress"
  remote_ip_prefix  = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_rule_v2" "master-rule3" {
  ethertype = "IPv4"
  security_group_id = openstack_networking_secgroup_v2.sec-gr-k8s-master.id
  direction         = "ingress"
  remote_group_id   = openstack_networking_secgroup_v2.sec-gr-k8s-worker.id
}

resource "openstack_networking_secgroup_rule_v2" "master-rule4" {
  ethertype = "IPv4"
  security_group_id = openstack_networking_secgroup_v2.sec-gr-k8s-master.id
  direction         = "ingress"
  remote_group_id   = openstack_networking_secgroup_v2.sec-gr-k8s-master.id
}

resource "openstack_networking_secgroup_rule_v2" "master-rule5" {
  ethertype = "IPv4"
  security_group_id = openstack_networking_secgroup_v2.sec-gr-k8s-master.id
  direction         = "ingress"
  remote_group_id   = "c70c415a-de02-4d84-9a1b-056707381dbe" #default security group 
}

#worker Security groups-> porturi deschise:  30000-32767 tcp de la oricine; ipv4 la oricine si ipv4 de la oricine din test-cluster-k8s(de la alte master-uri si noduri)
resource "openstack_networking_secgroup_rule_v2" "worker-rule1" {
  ethertype = "IPv4"
  security_group_id = openstack_networking_secgroup_v2.sec-gr-k8s-worker.id
  direction         = "ingress"
  protocol          = "tcp"
  port_range_min    = 30000
  port_range_max    = 32767
  remote_ip_prefix  = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_rule_v2" "worker-rule2" {
  ethertype = "IPv4"
  security_group_id = openstack_networking_secgroup_v2.sec-gr-k8s-worker.id
  direction         = "egress"
  remote_ip_prefix  = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_rule_v2" "worker-rule3" {
  ethertype = "IPv4"
  security_group_id = openstack_networking_secgroup_v2.sec-gr-k8s-worker.id
  direction         = "ingress"
  remote_group_id   = openstack_networking_secgroup_v2.sec-gr-k8s-worker.id
}

resource "openstack_networking_secgroup_rule_v2" "worker-rule4" {
  ethertype = "IPv4"
  security_group_id = openstack_networking_secgroup_v2.sec-gr-k8s-worker.id
  direction         = "ingress"
  remote_group_id   = openstack_networking_secgroup_v2.sec-gr-k8s-master.id
}

resource "openstack_networking_secgroup_rule_v2" "worker-rule5" {
  ethertype = "IPv4"
  security_group_id = openstack_networking_secgroup_v2.sec-gr-k8s-worker.id
  direction         = "ingress"
  remote_group_id   = "c70c415a-de02-4d84-9a1b-056707381dbe" #default security group
}

#bastion Security groups -> la oricine si de la workeri,masteri pe ipv4 si port 22
resource "openstack_networking_secgroup_rule_v2" "bastion-rule1" {
  direction         = "ingress"
  ethertype         = "IPv4"
  security_group_id = openstack_networking_secgroup_v2.sec-gr-bastion.id
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_rule_v2" "bastion-rule2" {
  direction         = "egress"
  ethertype         = "IPv4"
  security_group_id = openstack_networking_secgroup_v2.sec-gr-bastion.id
  remote_ip_prefix  = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_rule_v2" "bastion-rule3" {
  direction                = "ingress"
  ethertype                = "IPv4"
  security_group_id        = openstack_networking_secgroup_v2.sec-gr-bastion.id
  remote_group_id          = openstack_networking_secgroup_v2.sec-gr-k8s-master.id
}

resource "openstack_networking_secgroup_rule_v2" "bastion-rule4" {
  direction                = "ingress"
  ethertype                = "IPv4"
  security_group_id        = openstack_networking_secgroup_v2.sec-gr-bastion.id
  remote_group_id          = openstack_networking_secgroup_v2.sec-gr-k8s-worker.id
}

#NFS Security groups -> tcp/udp 2049,111 ingress si 22 doar de la bastion
resource "openstack_networking_secgroup_rule_v2" "nfs-rule1" {
  direction         = "ingress"
  ethertype         = "IPv4"
  security_group_id = openstack_networking_secgroup_v2.sec-gr-nfs-server.id
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_group_id   = openstack_networking_secgroup_v2.sec-gr-bastion.id
}

resource "openstack_networking_secgroup_rule_v2" "nfs-rule2" {
  direction         = "ingress"
  ethertype         = "IPv4"
  security_group_id = openstack_networking_secgroup_v2.sec-gr-nfs-server.id
  protocol          = "tcp"
  port_range_min    = 2049
  port_range_max    = 2049
  remote_ip_prefix  = "0.0.0.0/0"
}
resource "openstack_networking_secgroup_rule_v2" "nfs-rule3" {
  direction         = "ingress"
  ethertype         = "IPv4"
  security_group_id = openstack_networking_secgroup_v2.sec-gr-nfs-server.id
  protocol          = "tcp"
  port_range_min    = 111
  port_range_max    = 111
  remote_ip_prefix  = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_rule_v2" "nfs-rule4" {
  direction         = "ingress"
  ethertype         = "IPv4"
  security_group_id = openstack_networking_secgroup_v2.sec-gr-nfs-server.id
  protocol          = "udp"
  port_range_min    = 2049
  port_range_max    = 2049
  remote_ip_prefix  = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_rule_v2" "nfs-rule5" {
  direction         = "ingress"
  ethertype         = "IPv4"
  security_group_id = openstack_networking_secgroup_v2.sec-gr-nfs-server.id
  protocol          = "udp"
  port_range_min    = 111
  port_range_max    = 111
  remote_ip_prefix  = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_rule_v2" "nfs-rule6" {
  ethertype = "IPv4"
  security_group_id = openstack_networking_secgroup_v2.sec-gr-nfs-server.id
  direction         = "egress"
  remote_ip_prefix  = "0.0.0.0/0"
}