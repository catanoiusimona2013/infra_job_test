terraform {
required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.51.1"
    }
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "~> 3.0"
    }
  }
  backend "http" {
  }
}

provider "openstack" {
  auth_url      = "https://10.123.0.12:5000/v3"
  tenant_id    = "a0dca8a02333485c823f8a1d855e2408"
  tenant_name  = "catanoiuatm_project"
  user_domain_name = "SCHOLARSHIP_DOMAIN"
  project_domain_id = "5c0d3dfebef745d58a11e282b90ed700"
  user_name      = var.openstack_username
  password      = var.openstack_password
  cacert_file = var.openstack_cert_path
  region = "Brasov"
}

provider "gitlab" {
  token = var.gitlab_token
  base_url="https://www.licenta2024atm.com/gitlab"
}