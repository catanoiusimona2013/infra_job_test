#Create jmphost instance based on jump host instance
module "gitlab" {
    source="./modules/instance_module"
    instance_name="gitlab_vm"
    image_id="59b03698-e42c-4f00-8aeb-5e82aec71023" #Ubuntu
    image_username = "ubuntu"
    flavor_name="m1.medium" # zice ca are nevoide de minim 4Gb ram si 4 Cpu
    key_pair="gitlab_vm"
    instance_security_groups_name=["default"]
    script_path="/scripts/init.sh"
    port_name="gitlab_port"
    port_security_group_ids=["c70c415a-de02-4d84-9a1b-056707381dbe"] #default security group
    network_id=openstack_networking_network_v2.main_network.id
    subnet_id=openstack_networking_subnet_v2.workspace_subnet.id
    kubespray_groups = ""
}

output "gitlab_instance_ip" {
  value = module.jmphost.instance_ip
}
