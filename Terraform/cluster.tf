module "cluster" {
  source = "./modules/cluster_module"
  cluster_name = "k8s"
  network_name = "cluster_network"
  subnet_name  = "cluster_subnet"
  cidr         = "192.168.30.0/24"
  dns_values   = var.dns_values
  number_of_k8s_masters = 3
  number_of_k8s_nodes = 2
  public_key_path = "../VM_Private_Keys/cluster-key.pem.pub"

  #master 
  master_flavour = "m1.2xsmall"
  master_image_id = "59b03698-e42c-4f00-8aeb-5e82aec71023" #Ubuntu 20.04
  master_image_username = "ubuntu"
  master_security_group = [openstack_networking_secgroup_v2.sec-gr-k8s-master.name]
  master_port_security_group = [openstack_networking_secgroup_v2.sec-gr-k8s-master.id] #k8s-master security group
  master_kubespray_groups = "etcd,kube_control_plane,k8s_cluster,no_floating"
  master_script_path = "/scripts/init.sh"
  
  #worker
  workers_flavour= "m1.2xsmall"
  worker_image_id = "59b03698-e42c-4f00-8aeb-5e82aec71023" #Ubuntu 20.04
  worker_image_username = "ubuntu"
  worker_security_group = [openstack_networking_secgroup_v2.sec-gr-k8s-worker.name]
  worker_port_security_group = [openstack_networking_secgroup_v2.sec-gr-k8s-worker.id] #k8s-worker security group
  worker_kubespray_groups = "kube_node,k8s_cluster,no_floating"
  worker_script_path = "/scripts/init.sh"

  #nfs server
  nfs_flavour= "m1.2xsmall"
  nfs_image_id = "59b03698-e42c-4f00-8aeb-5e82aec71023" #Ubuntu 20.04
  nfs_image_username = "ubuntu"
  nfs_security_group = [openstack_networking_secgroup_v2.sec-gr-nfs-server.name]
  nfs_port_security_group = [openstack_networking_secgroup_v2.sec-gr-nfs-server.id] #nfs security group
  nfs_kubespray_groups = ""
  nfs_script_path = "/scripts/init.sh"
}

#crete router interface for cluster network
resource "openstack_networking_router_interface_v2" "cluster_network_interface" {
    router_id = openstack_networking_router_v2.main_network_router.id
    subnet_id = module.cluster.subnet_id
}

output "master_ips" {
  value = module.cluster.master_ips
}

output "worker_ips" {
  value = module.cluster.worker_ips
}

output "nfs_ip" {
  value = module.cluster.nfs_ip
}