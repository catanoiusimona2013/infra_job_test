#!/bin/bash

echo "Choose the option:"
echo "(d) Decrypt all Ansible vault files"
echo "(e) Encrypt all Ansible vault files"
read option

decrypt() {
  if [ ! -d "EncryptedData" ]; then
    echo "Error: EncryptedData directory not found."
    exit 1
  fi
  # Decrypt files
  if ansible-vault decrypt --ask-vault-pass EncryptedData/env.tfvars EncryptedData/backend.conf EncryptedData/env_var_enc.sh EncryptedData/VM_Private_Keys_enc/*; then
      # Move decrypted files
      mkdir -p VM_Private_Keys
      mv EncryptedData/env.tfvars Terraform/
      mv EncryptedData/backend.conf Terraform/
      mv EncryptedData/env_var_enc.sh env_var.sh
      mv EncryptedData/VM_Private_Keys_enc/* VM_Private_Keys/

      # Set permissions for files in VM_Private_Keys directory to 600
      chmod 600 VM_Private_Keys/*
    else
      echo "Decryption failed."
      exit 1
    fi
}

encrypt() {
  if [ ! -d "EncryptedData" ]; then
    mkdir EncryptedData
    mkdir EncryptedData/VM_Private_Keys_enc
  fi
  # Copy files before encryption
  cp Terraform/env.tfvars EncryptedData/
  cp Terraform/backend.conf EncryptedData/
  cp env_var.sh EncryptedData/env_var_enc.sh
  cp -R VM_Private_Keys/* EncryptedData/VM_Private_Keys_enc

  # Encrypt files
  ansible-vault encrypt --ask-vault-pass EncryptedData/env.tfvars EncryptedData/backend.conf EncryptedData/env_var_enc.sh EncryptedData/VM_Private_Keys_enc/*
}

case $option in
  d)
    decrypt
    ;;
  e)
    encrypt
    ;;
  *)
    echo "Wrong option"
    ;;
esac

